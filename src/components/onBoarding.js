import React from 'react';
import logo from '../logo.svg';
import '../App.css';

function onBoarding() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
        On Boarding Page
        </p>
        
      </header>
    </div>
  );
}

export default onBoarding;
